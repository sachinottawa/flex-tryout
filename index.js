const form = document.getElementById('form')
const boxHolder = document.getElementById('box-holder')

form['flex'].forEach(radioButton => {
    radioButton.addEventListener('change', () => {
      if (radioButton.value === 'flex') {
        // Handle the change event
        boxHolder.classList.add('flex')
      }
      else{
        boxHolder.classList.remove('flex')
      }
    });
  });

  form['flex-dir'].forEach(radioButton => {
    radioButton.addEventListener('change', () => {
      if (radioButton.value === 'flex-dir-row') {
        boxHolder.classList.remove('flex-dir-col')
        boxHolder.classList.add('flex-dir-row')
      }
      else{
        boxHolder.classList.remove('flex-dir-row')
        boxHolder.classList.add('flex-dir-col')
      }
    });
  });

  form['justify'].forEach(radioButton => {
    radioButton.addEventListener('change', () => {
        const value = radioButton.value
        console.log(value)
        const possibleValues = ['justify-flex-start', 'justify-flex-end', 'justify-center', 'justify-space-around', 'justify-space-between']
        possibleValues.forEach(val => {
            if(value !== val){
                boxHolder.classList.remove(val)
            }
        })
    
        boxHolder.classList.add(value)
    });
  });

  
  form['align'].forEach(radioButton => {
    radioButton.addEventListener('change', () => {
        const value = radioButton.value
        console.log(value)
        const possibleValues = ['align-flex-start', 'align-flex-end', 'align-center', 'align-baseline', 'align-stretch']
        possibleValues.forEach(val => {
            if(value !== val){
                boxHolder.classList.remove(val)
            }
        })
    
        boxHolder.classList.add(value)
    });
  });
  